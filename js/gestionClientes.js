var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
var clientesObtenidos;
function getClientes() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function(){
        if(this.readyState==4||this.status==200){
            //console.table(JSON.parse(request.responseText).value);
            clientesObtenidos = request.responseText;
            procesarClientes();

        }
    }

    request.open("GET", url, true);
    request.send();
    
}

function procesarClientes() {
    var JSONClientes = JSON.parse(clientesObtenidos);
    //alert(JSONProductos.value[0].Productname);
    var divTabla = document.getElementById("divClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-over");
    
    for (var i=0; i<JSONClientes.value.length; i++){
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;

        var columnaCiudad = document.createElement("td");
        columnaCiudad.innerText = JSONClientes.value[i].City;

        var columnaPais= document.createElement("td");
        var imgBandera = document.createElement("img");
        var pais = JSONClientes.value[i].Country;
        if(pais == "UK"){
          pais = "United-Kingdom";
        }
        imgBandera.src=rutaBandera + pais + ".png";
        imgBandera.classList.add("flag");
        columnaPais.appendChild(imgBandera);

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCiudad);
        nuevaFila.appendChild(columnaPais);

        tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);

    
}